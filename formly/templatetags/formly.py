from django import template

from ..forms.widgets import LikertSelect, RatingSelect
from formly.models import SurveyResult
register = template.Library()


@register.filter
def is_likert(field):
    return isinstance(field.field.widget, LikertSelect)


@register.filter
def is_rating(field):
    return isinstance(field.field.widget, RatingSelect)


@register.inclusion_tag('formly/student_actions.html')
def student_actions(user,survey):
    try:
        s = SurveyResult.objects.get(survey=survey, user=user)
        return {'survey':survey,'r':s, 'state':2 if s.completed() else 1}

    except SurveyResult.DoesNotExist:
        return {'state': 0,'survey':survey,}