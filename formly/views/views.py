import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404

from formly.models import Survey,SurveyResult
from formly.forms.forms import SurveyResultForm

from django.utils.timezone import datetime


# Create your views here.
log = logging.getLogger(__name__)


@login_required
def index(request):
    context = {
        'surveys':Survey.objects.order_by('-id')
    }

    return render(request,'index.html',context)

@login_required
def archive(request):
    # context = {
    #     'surveys':Survey.objects.order_by('-id')
    # }
    return render(request,'archives.html')




@login_required
def survey_start(request,pk):

    survey = get_object_or_404(Survey, pk=pk)
    if request.method == "POST":
        form = SurveyResultForm(request.POST)
        if form.is_valid():
            survey_result = form.save()

            return redirect('/question/surveys/{}/results/{}/run'.format(pk,survey_result.pk))

    else:
        form = SurveyResultForm(
            initial={
                'survey' : survey.pk,

                'date_submitted':datetime.now(),
                'user':request.user.pk
            }
        )


    # survey_result = SurveyResult(
    #     survey = survey,
    #     user=request.user,
    #     daycare = daycare
    # )
    # survey_result.save()
    # page = survey_result.next_page(user=request.user)

    # return redirect('/run/survey/{}/{}/'.format(daycare_pk, survey_result.pk))
    return render(request,'formly/survey/start.html',{'form':form})