from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404,redirect

from django.contrib.auth.decorators import login_required

from formly.models import Survey,SurveyResult
from formly.forms.run import CommentForm


@login_required
def survey_results(request, pk):

    # if not request.user.is_superuser:
    #     # redirect('/results/)
    #     raise PermissionDenied


    
    # if not request.user.is_superuser:
    #     # redirect('/results/)
    #     raise PermissionDenied


    survey = get_object_or_404(Survey, pk=pk)

    # if not request.user.has_perm("formly.view_results", obj=survey):
    #     raise PermissionDenied()

    field_result = survey.results.first()
    if field_result:
        survey_result = field_result.result
    else:
        survey_result = SurveyResult.objects.none()
        # SurveyResult.objects.get_or_create(
        #     survey=field.survey,
        #     user=user,
        #     daycare=daycare
        # )

    if request.method == "POST":
        form = CommentForm(request.POST)
        print(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.creator = request.user

            field_result = survey.results.first()
            comment.survey=field_result.result
            #comment.text = form.cleaned_data['text'].strip()
            comment.survey = survey_result
            comment.save()

            return redirect('/question/results/survey/{}/'.format(survey.pk))
        else:
            print(form.errors)

    else:
        form = CommentForm(request.POST,request.FILES)
        print(form.errors)

    return render(request, "formly/results/home.html", {
        "survey": survey,
        "survey_result":survey_result,
        "form":form
    })


def results(request,pk):
    survey = get_object_or_404(Survey, pk=pk)

    # if not request.user.has_perm("formly.view_results", obj=survey):
    #     raise PermissionDenied()

    return render(request, "formly/results/home2.html", {
        "survey": survey,
        "results": survey.survey_results.filter(is_completed=True).order_by('-id'),
        # "form": form
    })


def results_draft(request,pk):
    survey = get_object_or_404(Survey, pk=pk)

    # if not request.user.has_perm("formly.view_results", obj=survey):
    #     raise PermissionDenied()

    return render(request, "formly/results/home2.html", {
        "survey": survey,
        "results": survey.survey_results.filter(is_completed=False,user=request.user).order_by('-id'),
        # "form": form
    })


def result(request,pk,result_pk):
    survey = get_object_or_404(Survey, pk=pk)

    # if not request.user.has_perm("formly.view_results", obj=survey):
    #     raise PermissionDenied()

    # field_result = survey.results.first()
    # if field_result:
    #     survey_result = field_result.result
    # else:
    survey_result = SurveyResult.objects.get(pk=result_pk)

    if request.method == "POST":
        form = CommentForm(request.POST)
        print(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.creator = request.user

            field_result = survey.results.first()
            comment.survey = field_result.result
            # comment.text = form.cleaned_data['text'].strip()
            comment.survey = survey_result
            comment.save()

            return redirect('/results/survey/{}/'.format(survey.pk))
        else:
            print(form.errors)

    else:
        form = CommentForm(request.POST, request.FILES)
        print(form.errors)

    return render(request, "formly/results/home.html", {
        "survey": survey,
        "survey_result": survey_result,
        "form": form
    })


def result_detail(request,pk,result_pk,result_page_pk=None):
    survey = get_object_or_404(Survey, pk=pk)

    # if not request.user.has_perm("formly.view_results", obj=survey):
    #     raise PermissionDenied()

    if not result_page_pk:
        results_page = survey.pages.first()
    else:
        results_page = survey.pages.get(pk=result_page_pk)

    results = results_page.results.filter(result_id=result_pk)
    survey_result = SurveyResult.objects.get(pk=result_pk)

    return render(request, "formly/results/page.html", {
        "survey": survey,
        "survey_result": survey_result,
        "results": results,
        "results_page": results_page
    })