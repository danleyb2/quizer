from django import forms
from formly.models import SurveyResult


class SurveyResultForm(forms.ModelForm):

    # survey = forms.C(widget=forms.HiddenInput())
    # user = forms.ModelChoiceField(widget=forms.HiddenInput())
    # daycare = forms.ModelChoiceField(widget=forms.HiddenInput())
    # date_submitted = forms.DateField(widget=forms.HiddenInput())

    class Meta:
        model = SurveyResult
        exclude = ()