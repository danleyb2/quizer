from django import forms

from formly.models import SurveyResult, Field, FieldResult, Comment

import json
class FieldResultMixin(object):

    def save_result(self, field,survey_result, user):
        if not hasattr(self, "_survey_result"):
            self._survey_result = survey_result

            # self._survey_result, _ = SurveyResult.objects.get_or_create(
            #     survey=field.survey,
            #     user=user,
            # )

        if field.field_type == Field.MEDIA_FIELD:
            defaults = {"answer": {"answer": ""}, "upload": self.cleaned_data[field.unique_name]}
        else:
            defaults = {"answer": {"answer": self.cleaned_data[field.unique_name]}, "upload": ""}

        qs = FieldResult.objects.filter(
            survey=field.survey,
            result=self._survey_result,
            question=field
        )
        if qs.exists():
            result = qs.get()
            result.answer = json.dumps(defaults["answer"])
            result.upload = defaults["upload"]
            result.save()
        else:
            result = FieldResult.objects.create(
                survey=field.survey,
                page=field.page,
                result=self._survey_result,
                question=field,
                answer=json.dumps(defaults["answer"]),
                upload=defaults["upload"]
            )
        return result


class PageForm(FieldResultMixin, forms.Form):

    def __init__(self, *args, **kwargs):
        self.page = kwargs.pop("page")
        super(PageForm, self).__init__(*args, **kwargs)
        for field in self.page.fields.all():

            self.fields[ field.unique_name] = field.form_field()
            self.fields[ field.unique_name].widget.attrs['field_type'] = field.field_type
            targets = field.choices.filter(target__isnull=False)
            if targets.count() > 0:
                self.fields[field.unique_name].widget.attrs["data-reveal"] = ",".join([
                    "{0}".format(target.pk) for target in targets
                ])
                for target in targets:
                    self.fields[target.target.unique_name] = target.target.form_field()
                    self.fields[target.target.unique_name].widget.attrs["class"] = "hide"
                    self.fields[target.target.unique_name].widget.attrs["data-reveal-id"] = target.pk

    def save(self,survey_result, user):
        for field in self.page.fields.all():
            self.save_result(field,survey_result, user)


class TargetForm(FieldResultMixin, forms.Form):

    def __init__(self, *args, **kwargs):
        self.target = kwargs.pop("choice").target
        super(TargetForm, self).__init__(*args, **kwargs)
        self.fields[self.target.unique_name] = self.target.form_field()

    def save(self, user):
        return self.save_result(self.target, user)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)
