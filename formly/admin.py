from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register([
    OrdinalScale,
    OrdinalChoice,
    Survey,
    Page,
    FieldChoice,
    SurveyResult,
    FieldResult,
    Comment
])


class FieldAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Field._meta.fields]
    list_filter = ('page',)


admin.site.register(Field,FieldAdmin)