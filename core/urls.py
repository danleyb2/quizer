from __future__ import unicode_literals
from django.conf.urls import url, include
from . import views

app_name = 'core'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^student$', views.students, name='students'),
    url(r'^student/add$', views.create_student, name='student_add'),


    url(r'^teacher$', views.teachers, name='teacher'),
    url(r'^teacher/(?P<pk>\d+)/resend_email$', views.invite_teacher, name='teacher_invite'),
    url(r'^teacher/add$', views.create_teacher, name='teacher_add'),
    url(r'^teacher/add_success$', views.create_teacher_success, name='teacher_add_success'),

]