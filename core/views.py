from django.http import HttpResponseRedirect
from django.shortcuts import render
from authentication.models import User

# Create your views here.


def index(request):
    context = {}

    teachers = User.objects.filter(is_staff=True)
    students = User.objects.filter(is_staff=False,is_superuser=False)
    # teachers = User.objects.filter(is_staff=True)
    from formly.models import Survey
    questions = Survey.objects.all()

    context['teachers'] = teachers
    context['students'] = students
    context['questions'] = questions
    return render(request, 'core/dashboard.html',context)


def teachers(request):
    context = {}

    teacher_list = User.objects.filter(is_staff=True).order_by('-id')
    context['teachers'] = teacher_list

    return render(request, 'core/teachers.html',context)


def students(request):
    context = {}

    student_list = User.objects.filter(is_staff=False)
    context['students'] = student_list

    return render(request, 'core/students.html',context)


from authentication.forms import AdminRegisterUserForm


def create_teacher(request):
    context = {}

    # If this is a POST request then process the Form data
    if request.method == 'POST':
        account_form = AdminRegisterUserForm(request.POST, request.FILES)
        account_form.access_level = User.TEACHER
        if account_form.is_valid():
            account = account_form.save()

            return HttpResponseRedirect('/teacher/add_success')

        else:
            print(request.POST)
            print(account_form.errors)
            # context['form'] = form

    # If this is a GET (or any other method) create the default form.
    else:
        account_form = AdminRegisterUserForm()
        context['form'] = account_form

    return render(request, 'core/teacher_create.html',context)


def invite_teacher(request,pk):
    teacher = User.objects.get(pk=pk)
    context = {}

    account_form = AdminRegisterUserForm()

    account_form.invite(teacher)

    return render(request, 'core/teacher_invite_success.html',context)


from authentication.forms import StudentRegistrationForm


def create_student(request):
    context = {}

    # If this is a POST request then process the Form data
    if request.method == 'POST':
        form = StudentRegistrationForm(request.POST, request.FILES)
        form.access_level = User.STUDENT

        if form.is_valid():
            student = form.save()

            return HttpResponseRedirect('/student')

        else:
            print(request.POST)
            print(form.errors)
            # context['form'] = form

    # If this is a GET (or any other method) create the default form.
    else:
        form = StudentRegistrationForm()
        context['form'] = form

    return render(request, 'core/student_create.html',context)

def create_teacher_success(request):
    return render(request,'core/user_create_success.html')