#!/usr/bin/env python

import os

base_dir = os.path.dirname(os.path.abspath(__file__))

for subdir, dirs, files in os.walk(base_dir):
    if subdir.endswith('migrations'):
        for file in files:
            if file != '__init__.py':
                migration = os.path.join(subdir, file)
                print('Delete {}'.format(migration))
                os.remove(migration)
