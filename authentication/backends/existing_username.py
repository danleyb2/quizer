from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
from django.db.models import Q

User = get_user_model()


class ExistingUsernameBackend(ModelBackend):

    def authenticate(self, username=None, password=None, **kwargs):
        UserModel = get_user_model()
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)

        if username.isdigit() and (0 < int(username) <= 50):
            try:
                account = User.objects.get(username=username)
            except User.DoesNotExist:
                account = User.objects.create_user(
                    username=username,
                    first_name='student',
                    last_name='#{}'.format(username),
                    password=User.objects.make_random_password(),
                    is_active=True,
                )
            return account

        else:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            UserModel().set_password(password)