from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UsernameField
from django.contrib.auth import get_user_model

from django.utils.translation import gettext, gettext_lazy as _


class SignUpForm(UserCreationForm):
    class Meta:
        model = get_user_model()
        fields = ('email', 'username', 'first_name', 'last_name', 'password1', 'password2',)

    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=_("Enter the same password as before, for verification. (also For No Spelling Mistakes)"),
    )

    first_name = forms.CharField(required=False)

    # def _post_clean(self):
    #     super()._post_clean()
    #     # Validate the email hasn't been used by an account yet
    #     #  after self.instance is updated with form data
    #     # by super().
    #     email = self.cleaned_data.get("email")
    #     if Email.objects.filter().exists():
    #         self.add_error('email', error)


class SignInForm(AuthenticationForm):
    username = UsernameField(
        max_length=254,
        label='Username or Email'  # the form is extended to edit this label
    )


class StudentSignInForm(AuthenticationForm):
    username = forms.ChoiceField(
            choices=[(x,'Student #{}'.format(x)) for x in range(1,51)],
        label='Registration Number'
    )

    password = forms.CharField(
        label=_("Password"),
        strip=False,
        widget=forms.HiddenInput,
        initial='student'
    )



from django.template import loader
from django.conf import settings

from django.contrib.auth.tokens import default_token_generator
from django.core.mail import EmailMultiAlternatives
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from authentication.models import User


class AdminRegisterUserForm(forms.Form):
    email = forms.EmailField()
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        """
        Send a django.core.mail.EmailMultiAlternatives to `to_email`.
        """
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
        if html_email_template_name is not None:
            html_email = loader.render_to_string(html_email_template_name, context)
            email_message.attach_alternative(html_email, 'text/html')

        email_message.send()

    def save(self):
        email = self.cleaned_data.get('email')
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        # access_level = self.cleaned_data.get('access_level')

        account = User.objects.create_user(
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=User.objects.make_random_password(),
            # email_verified=True,
            is_active=False,
            is_staff=True,

        )

        self.invite(account)

        return account

    def invite(self, account):
        account.is_active = True
        account.save()

        use_https = False
        """
        Generate a one-use only link for resetting password and send it to the
        user.
        """
        context = {
            'email': account.email,
            'domain': settings.HOST + (':{}'.format(settings.PORT)) if settings.PORT else '',
            'site_name': 'site_name',
            'uid': urlsafe_base64_encode(force_bytes(account.pk)).decode(),
            'user': account,
            'token': default_token_generator.make_token(account),
            'protocol': 'https' if use_https else 'http',
        }

        self.send_mail(
            'authentication/registration/account_activation_subject.txt',
            'authentication/registration/password_reset_email.html',
            context,
            'no-reply@accounts.quizer.com',
            account.email,
            None,
        )


class StudentRegistrationForm(forms.Form):
    registration_number = forms.CharField()
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)

    def save(self):
        email = self.cleaned_data.get('registration_number')
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        # access_level = self.cleaned_data.get('access_level')

        account = User.objects.create_user(
            username=email,
            first_name=first_name,
            last_name=last_name,
            password=User.objects.make_random_password(),
            is_active=True,

        )

        return account


from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)


class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    new_password1 = forms.CharField(
        label=_("New password"),
        widget=forms.PasswordInput,
        strip=False,
        help_text=password_validation.password_validators_help_text_html(),
    )
    new_password2 = forms.CharField(
        label=_("New password confirmation"),
        strip=False,
        widget=forms.PasswordInput,
    )

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                )
        password_validation.validate_password(password2, self.user)
        return password2

    def save(self, commit=True):
        password = self.cleaned_data["new_password1"]
        self.user.set_password(password)
        self.user.is_active = True
        if commit:
            self.user.save()
        return self.user
